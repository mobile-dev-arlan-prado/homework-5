package edu.sjsu.android.stockmarket;

public class FavoriteStock {
    private String ticker;
    private String name;
    private Double price;
    private Double change;
    private String marketCap;
    private int isGain; //0 = no change, 1 = gain, -1 = loss

    public int getIsGain() {
        return isGain;
    }

    public void setIsGain(int isGain) {
        this.isGain = isGain;
    }

    public void setMarketCap(double marketCap) {
        if (marketCap > 1000000000) {
            this.marketCap = String.format("%.2f Billion", marketCap / 1000000000);
        } else if (marketCap > 1000000) {
            this.marketCap = String.format("%.2f Million", marketCap / 1000000);
        } else if (marketCap > 1000) {
            this.marketCap = String.format("%.2f Thousand", marketCap / 1000);
        } else {
            this.marketCap = String.format("%.2f", marketCap);
        }
    }

    public String getTicker() {
        return ticker;
    }

    public void setTicker(String ticker) {
        this.ticker = ticker;
    }

    public String getName() {
        String upToNCharacters = name.substring(0, Math.min(name.length(), 10));
        return upToNCharacters;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice() {
        return  String.format("%.2f",price);
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getChange() {
        return String.format("%.2f", change);
    }

    public Double getChangeDouble(){
        return change;
    }
    public void setChange(double change) {
        this.change = change;
    }

    public String getMarketCap() {
        return marketCap;
    }

    public void setMarketCap(String marketCap) {
        this.marketCap = marketCap;
    }

    public FavoriteStock(String ticker, String name, Double price, Double change, int isGain) {
        this.ticker = ticker;
        this.name = name;
        this.price = price;
        this.change = change;
        this.isGain = isGain;
    }
}
