package edu.sjsu.android.stockmarket;
//API 29

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import java.util.concurrent.TimeUnit;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.format.Time;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.RadioGroup;
import android.widget.Switch;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity {
    RequestQueue requestQueue;
    List<String> stockSuggestions;
    ArrayAdapter<String> adapter;
    AutoCompleteTextView stockText;
    Button clearButton;
    Button quoteButton;
    SharedPreferences preferences;
    RecyclerView favRecyclerView;
    public static FavoriteStockAdapter favoriteStockAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        requestQueue = Volley.newRequestQueue(this);

        //clear button
        clearButton = (Button) findViewById(R.id.clearButton);
        clearButton.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View view) {
                stockText.setText("");
            }
        });
        //quote button
        quoteButton = (Button) findViewById(R.id.quoteButton);
        quoteButton.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View view) {
                Log.d("quote", "quote called");
                String ticker = stockText.getText().toString();
                if(ticker.isEmpty()){
                    AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
                    alertDialog.setMessage("No Stock Called");
                    alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    alertDialog.show();
                }else {

                    Intent i = new Intent(MainActivity.this, StockActivity.class);
                    i.putExtra("ticker", ticker);

//                    Intent i = new Intent(MainActivity.this, CurrentStockFragment.class);
//                    i.putExtra("ticker", ticker);
                    CurrentStockFragment currentStockFragment = new CurrentStockFragment();
                    Bundle bundle = new Bundle();
                    Log.d("bundle", "ticker SENT: " + ticker);
                    bundle.putString("ticker", ticker);
                    currentStockFragment.setArguments(bundle);

                    startActivity(i);
                }
            }
        });
        //autocompletetextview
        stockText = (AutoCompleteTextView) findViewById(R.id.autoCompleteTextView);
        stockText.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String selected = (String) adapterView.getItemAtPosition(i);
                stockText.setText(selected);
            }
        });
//        stockSuggestions = new ArrayList<String>();
        stockText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                Log.d("auto", "auto complete called");
                getStockSuggestions(charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        //auto refresh switch
        Switch autoRefreshSwitch = (Switch) findViewById(R.id.autoRefreshSwitch);
        final Handler handler = new Handler();
        final Runnable runnable = new Runnable() {
            @Override
            public void run() {
                refreshFavorites();
                handler.postDelayed(this, 10000);
            }
        };
        autoRefreshSwitch.setOnCheckedChangeListener(new Switch.OnCheckedChangeListener(){
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, final boolean b) {
                Log.d("Main-autoRefresh","SWITCHED");
                if(b)
                    handler.postDelayed(runnable, 10000);
                else
                    handler.removeCallbacks(runnable);
            }
        });
        //refreshbutton
        ImageButton refreshFavButton = (ImageButton) findViewById(R.id.refreshImageButton);
        refreshFavButton.setOnClickListener(new ImageButton.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("Main-refreshFavButton", "CLICKED");
                refreshFavorites();
            }
        });
        //favlist
        preferences = this.getSharedPreferences("edu.sjsu.android.stockmarket", Context.MODE_PRIVATE);
        favRecyclerView = findViewById(R.id.favoritesList);
        favRecyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    //refresh favorite stock list
    private void refreshFavorites(){
        Log.d("Main-refreshFavorites","triggered");

        Map<String, ?> favoriteStocks = preferences.getAll();
        if(favoriteStocks.isEmpty())
            return;
        Log.d("Main-refreshFavorites",favoriteStocks.toString());
        FavoriteStockAdapter.favoriteStockArrayList.clear();
        if(favoriteStockAdapter == (null)) {
            Log.d("Main-refreshFavorites","adapter is null");
            favoriteStockAdapter = new FavoriteStockAdapter();
            favRecyclerView.setAdapter(favoriteStockAdapter);
        }else
            favoriteStockAdapter.notifyDataSetChanged();

        for(Map.Entry<String, ?> fav : favoriteStocks.entrySet()){
            String s = fav.getKey();
            String token = "3ed1f372b5a83348867fe3a87aca6d1b2e569ab2";
            String nameUrl = "https://api.tiingo.com/tiingo/daily/" + s + "?token=" + token;
            final String[] tickerInfo = new String[2];
            final Double[] tickerDouble = new Double[3];
            final Integer[] tickerInt = new Integer[1];
            JsonObjectRequest arrayRequest1 = new JsonObjectRequest(nameUrl, null,
                    new Response.Listener<JSONObject>(){
                        @Override
                        public void onResponse(JSONObject response) {
//                            progressBar.setVisibility(View.INVISIBLE);
                            try {
//                            for(int i =0; i < response.length(); i++) {

                                //JSONObject jsonObject = response.getJSONObject(0);
                                JSONObject jsonObject = response;
                                tickerInfo[0] = jsonObject.get("name").toString();
                                if(tickerInfo[1] != null) {
                                    FavoriteStock favoriteStock = new FavoriteStock(tickerInfo[1], tickerInfo[0], tickerDouble[1], tickerDouble[2], tickerInt[0]);                                    favoriteStock.setMarketCap(tickerDouble[0] * tickerDouble[1]);
                                    FavoriteStockAdapter.favoriteStockArrayList.add(favoriteStock);
                                    favoriteStockAdapter.notifyDataSetChanged();

                                    Log.d("Main-Activity", "finished generating for " + jsonObject.get("ticker").toString() + " " + tickerInfo[0]);
                                }
                            } catch (JSONException e) {

                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener(){
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });
            Log.d("MainActivity","added1 to requestQueue");
            requestQueue.add(arrayRequest1);
            //###########
            String url = "https://api.tiingo.com/iex/?tickers=" + s + "&token=" + token;
            Log.d("quote", "about to reach onResponse");
            JsonArrayRequest arrayRequest = new JsonArrayRequest(url,
                    new Response.Listener<JSONArray>(){
                        @Override
                        public void onResponse(JSONArray response) {
//                            progressBar.setVisibility(View.INVISIBLE);

                            Log.d("MainActivity", "response TICKER, CHANGE generated" + response.toString());
                            try {
//                            for(int i =0; i < response.length(); i++) {

                                JSONObject jsonObject = response.getJSONObject(0);
                                tickerDouble[0] = jsonObject.getDouble("volume");
                                double prevClose = jsonObject.getDouble("prevClose");
                                tickerDouble[1] = jsonObject.getDouble("last");
                                if(prevClose != tickerDouble[1]){
                                    tickerDouble[2] = ((tickerDouble[1] - prevClose)/prevClose)*100;
                                    if(tickerDouble[2] < 0){
                                        tickerDouble[2] = (tickerDouble[2] * 2) - tickerDouble[2];
                                        tickerInt[0] = -1;
                                    }else
                                        tickerInt[0] = 1;
                                }else
                                    tickerDouble[2] = 0.0;

                                tickerInfo[1] = jsonObject.get("ticker").toString();
                                if(tickerInfo[0] != null) {
                                    FavoriteStock favoriteStock = new FavoriteStock(tickerInfo[1], tickerInfo[0], tickerDouble[1], tickerDouble[2], tickerInt[0]);
                                    favoriteStock.setMarketCap(tickerDouble[0] * tickerDouble[1]);
                                    FavoriteStockAdapter.favoriteStockArrayList.add(favoriteStock);
                                    favoriteStockAdapter.notifyDataSetChanged();

                                    Log.d("Main-Activity", "finished generating for " + jsonObject.get("ticker").toString() + " " + tickerInfo[0]);
                                }
//                            }
                            } catch (JSONException e) {

                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener(){
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });

            Log.d("MainActivity","added to requestQueue");
            requestQueue.add(arrayRequest);
        }
    }
    @Override
    protected void onResume() {
        super.onResume();
        refreshFavorites(); // When you pause and then resume the app, you want to refresh the stocks
    }
    //autocomplete suggestion list
    private void getStockSuggestions(String s) {
        String token = "3ed1f372b5a83348867fe3a87aca6d1b2e569ab2";
        String url = "https://api.tiingo.com/tiingo/utilities/search?query=" + s + "&token=" + token;
        stockSuggestions = new ArrayList<String>();
        JsonArrayRequest arrayRequest = new JsonArrayRequest(url, new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        try{
                            for(int i=0; i < response.length(); i++){
                                JSONObject jsonObject = response.getJSONObject(i);
                                String ticker = jsonObject.getString("ticker");
                                stockSuggestions.add(ticker);
                            }
                            adapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_dropdown_item_1line, stockSuggestions);
                            stockText.setAdapter(adapter);
                            adapter.notifyDataSetChanged();
                        } catch (JSONException e){
                            e.printStackTrace();
                        }
                    }
        }, new Response.ErrorListener(){

            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        requestQueue.add(arrayRequest);
    }


}