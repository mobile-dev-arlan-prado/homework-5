package edu.sjsu.android.stockmarket;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class HistoryStockAdapter extends RecyclerView.Adapter {
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.stock_history_row, parent, false);
        return new HistoryStockViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ((HistoryStockViewHolder) holder).bindView(position);
    }

    @Override
    public int getItemCount() {
        return StockHistoryList.historyArrayList.size();
    }

    private class HistoryStockViewHolder extends RecyclerView.ViewHolder {
        private TextView date;
        private TextView closePrice;
        private TextView volume;

        public HistoryStockViewHolder(View itemView){
            super(itemView);
            date = (TextView) itemView.findViewById(R.id.date);
            closePrice = (TextView) itemView.findViewById(R.id.closePrice);
            volume = (TextView) itemView.findViewById(R.id.volume);
        }

        public void bindView(int position){
//            Log.d("bind","binded called " + );

            date.setText(date.getText() + StockHistoryList.historyArrayList.get(position).getDate());
            closePrice.setText(closePrice.getText() + "$" + StockHistoryList.historyArrayList.get(position).getClosePrice());
            volume.setText(volume.getText() + StockHistoryList.historyArrayList.get(position).getVolume());
        }
    }
}
