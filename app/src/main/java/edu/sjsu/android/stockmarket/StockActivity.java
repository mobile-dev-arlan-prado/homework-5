package edu.sjsu.android.stockmarket;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ProgressBar;
import android.widget.ToggleButton;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Cache;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Network;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;
import java.util.Objects;

public class StockActivity extends AppCompatActivity {
    RequestQueue requestQueue;
    ProgressBar progressBar;
    public static String ticker;
    Button currentButton;
    Button historyButton;
    ToggleButton favButton;
    SharedPreferences sharedPreferences;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stock_detail);
//
//        progressBar = (ProgressBar) findViewById(R.id.progress_circular);
//        progressBar.setVisibility(View.VISIBLE);
//
//
//        String ticker = getIntent().getStringExtra("ticker");
//        requestQueue = Volley.newRequestQueue(getApplicationContext());
//
//
//        Log.d("ticker request", "\""+ticker+"\"");
//        setStockInfo(ticker);


//        final HistoryStockFragment historyStockFragment = new HistoryStockFragment();
//
//        FragmentManager fragmentManager = getSupportFragmentManager();
//        final FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        ticker = getIntent().getStringExtra("ticker");

//        Bundle bundle = new Bundle();
//        Log.d("bundle","ticker: " + ticker);
//        bundle.putString("ticker", ticker);
//        CurrentStockFragment currentStockFragment = new CurrentStockFragment();
//        currentStockFragment.setArguments(bundle);
//        historyStockFragment.setArguments(bundle);

//        fragmentTransaction.add(R.id.fragment_container_view, currentStockFragment);
//        fragmentTransaction.addToBackStack(null);
//        fragmentTransaction.commit();

        CurrentStockFragment currentStockFragment = new CurrentStockFragment();
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.fragment_container_view, currentStockFragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();

        currentButton = (Button) findViewById(R.id.currentStockButtonFrag);
        currentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CurrentStockFragment currentStockFragment = new CurrentStockFragment();
                FragmentManager fragmentManager = getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.fragment_container_view, currentStockFragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });
        historyButton = (Button) findViewById(R.id.historyStockButtonFrag);
        historyButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                HistoryStockFragment historyStockFragment = new HistoryStockFragment();
                FragmentManager fragmentManager = getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.fragment_container_view, historyStockFragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });
        requestQueue = Volley.newRequestQueue(getApplicationContext());
        sharedPreferences = Objects.requireNonNull(this).getSharedPreferences("edu.sjsu.android.stockmarket", Context.MODE_PRIVATE);
        Map<String,?> map = sharedPreferences.getAll();
        //favbutton
        favButton = findViewById(R.id.favoriteButton);
        if(map.containsKey(ticker)){
            favButton.setChecked(true);
        }else{
            favButton.setChecked(false);
        }
        favButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    Log.d("StockActivity-favButton","request to add favorite");
                    addFavorite(ticker);
                }else if(!b){
                    Log.d("StockActivity-favButton","request to remove favorite");
                    removeFavorite(ticker);
                }
            }
        });
    }
    private void removeFavorite(String s){
        Log.d("stockActivity-removeFavorite", "called");
        for(Map.Entry<String, ?> temp: sharedPreferences.getAll().entrySet()){
            if(temp.getKey().equals(s)){
                Log.d("stockActivity-removeFavorite", "found and removed successfully");
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.remove(s);
                editor.apply();
            }
        }
    }
    private void addFavorite(String s){
        String token = "3ed1f372b5a83348867fe3a87aca6d1b2e569ab2";
        String nameUrl = "https://api.tiingo.com/tiingo/daily/" + s + "?token=" + token;
        final String[] name = new String[1];
        JsonObjectRequest arrayRequest1 = new JsonObjectRequest(nameUrl, null,
                new Response.Listener<JSONObject>(){
                    @Override
                    public void onResponse(JSONObject response) {
//                            progressBar.setVisibility(View.INVISIBLE);
                        try {
//                            for(int i =0; i < response.length(); i++) {

                            //JSONObject jsonObject = response.getJSONObject(0);
                            JSONObject jsonObject = response;
                            name[0] = jsonObject.get("name").toString();
                            Log.d("MainActivity", "response NAME generated" + name[0]);
//                            }
                        } catch (JSONException e) {

                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        Log.d("MainActivity","added1 to requestQueue");
        requestQueue.add(arrayRequest1);
        //###########
        String url = "https://api.tiingo.com/iex/?tickers=" + s + "&token=" + token;
        Log.d("quote", "about to reach onResponse");
        JsonArrayRequest arrayRequest = new JsonArrayRequest(url,
                new Response.Listener<JSONArray>(){
                    @Override
                    public void onResponse(JSONArray response) {
//                        progressBar.setVisibility(View.INVISIBLE);

                        Log.d("stockActivity", "response TICKER, CHANGE generated" + response.toString());
                        try {
//                            for(int i =0; i < response.length(); i++) {

                            JSONObject jsonObject = response.getJSONObject(0);
                            double volume = jsonObject.getDouble("volume");
                            double prevClose = jsonObject.getDouble("prevClose");
                            double close = jsonObject.getDouble("last");
                            double change;
                            int isGain = 0;
                            if(prevClose != close){
                                change = ((close - prevClose)/prevClose)*100;
                                if(change < 0){
                                    change = (change * 2) - change;
                                    isGain = -1;
                                }else
                                    isGain = 1;
                            }else
                                change = 0;

                            FavoriteStock favoriteStock = new FavoriteStock(jsonObject.get("ticker").toString(), name[0], jsonObject.getDouble("last"), change, isGain);
                            favoriteStock.setMarketCap(volume * close);
                            FavoriteStockAdapter.favoriteStockArrayList.add(favoriteStock);
                            SharedPreferences.Editor editor = sharedPreferences.edit();
                            editor.putString(jsonObject.get("ticker").toString(), jsonObject.get("ticker").toString());
                            editor.apply();
//                            }
                        } catch (JSONException e) {

                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
//        arrayRequest.setRetryPolicy(new DefaultRetryPolicy(
//                20000,
//                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
//                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.d("stockActivity","added to requestQueue");
        requestQueue.add(arrayRequest);
    }
}
