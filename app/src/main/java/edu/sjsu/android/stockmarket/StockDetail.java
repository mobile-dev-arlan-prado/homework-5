package edu.sjsu.android.stockmarket;

import java.util.ArrayList;

public class StockDetail {
    public static ArrayList<String> labelList= new ArrayList<String>();
    public static ArrayList<String> contentList= new ArrayList<String>();

//    public StockDetail(){
//        labelList = new ArrayList<String>();
//        contentList = new ArrayList<String>();
//    }


    public static void clear(){
        labelList.clear();
        contentList.clear();
    }

    public static void add(String label, String content){
        labelList.add(label);
        contentList.add(content);
    }

    public String getLabelAt(int pos){
        return labelList.get(pos);
    }

    public String getContentAt(int pos){
        return contentList.get(pos);
    }

    public static int getLength(){
        return labelList.size();
    }


}
