package edu.sjsu.android.stockmarket;

import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class FavoriteStockAdapter extends RecyclerView.Adapter {
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Log.d("FSA", "Adapter Called");
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.favorite_stock_row, parent, false);
        return new FavoriteStockViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ((FavoriteStockViewHolder) holder).bindView(position);
    }

    @Override
    public int getItemCount() {
        return favoriteStockArrayList.size();
    }
    public static ArrayList<FavoriteStock> favoriteStockArrayList = new ArrayList<FavoriteStock>();
    private class FavoriteStockViewHolder extends RecyclerView.ViewHolder {
        public TextView ticker;
        public TextView name;
        public TextView price;
        public TextView change;
        public TextView marketCap;

        public FavoriteStockViewHolder(View itemView){
            super(itemView);
            ticker = (TextView) itemView.findViewById(R.id.fav_stock_ticker);
            name = (TextView) itemView.findViewById(R.id.fav_stock_name);
            price = (TextView) itemView.findViewById(R.id.fav_stock_price);
            change = (TextView) itemView.findViewById(R.id.fav_stock_change);
            marketCap = (TextView) itemView.findViewById(R.id.fav_stock_mcap);
        }

        public void bindView(int position){
            Log.d("FSA","binded called " + favoriteStockArrayList.get(position).getTicker());

            ticker.setText(favoriteStockArrayList.get(position).getTicker());
            name.setText(favoriteStockArrayList.get(position).getName());
            price.setText("$"+favoriteStockArrayList.get(position).getPrice());
            change.setText(favoriteStockArrayList.get(position).getChange()+"%");
            marketCap.setText("Market Cap: " + favoriteStockArrayList.get(position).getMarketCap());

            double isGain = favoriteStockArrayList.get(position).getIsGain();
            if(isGain > 0){
                change.setBackgroundColor(Color.GREEN);
            }else if(isGain < 0){
                change.setBackgroundColor(Color.RED);
            }else{
                change.setBackgroundColor(Color.WHITE);
            }
        }
    }
}
