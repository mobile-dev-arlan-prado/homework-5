package edu.sjsu.android.stockmarket;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Objects;
import java.util.concurrent.TimeUnit;


public class CurrentStockFragment extends Fragment {
    RequestQueue requestQueue;
    ProgressBar progressBar;
    Boolean isDataReady;
    View view;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d("currentFragment", "CSF called");
        view = inflater.inflate(R.layout.fragment_current_stock, container, false);

        progressBar = (ProgressBar) getActivity().findViewById(R.id.progress_circular);
        progressBar.setVisibility(View.VISIBLE);
        isDataReady = false;
        requestQueue = Volley.newRequestQueue(getActivity().getApplicationContext());
//        Log.d("bundle received", "result: " + getArguments().getString("ticker"));
//        String ticker = getArguments().getString("ticker");
//        Log.d("ticker request", "\""+ticker+"\"");
        String ticker = StockActivity.ticker;
        Log.d("ticker request", "\""+ticker+"\"");
        StockDetail.clear();
        setStockInfo(ticker);
        Log.d("stockdetail", "outside of response");

        return view;
    }
    private void setStockInfo(String s){
        String token = "3ed1f372b5a83348867fe3a87aca6d1b2e569ab2";
        String url = "https://api.tiingo.com/iex/?tickers=" + s + "&token=" + token;
        Log.d("quote", "about to reach onResponse");
        JsonArrayRequest arrayRequest = new JsonArrayRequest(url,
                new Response.Listener<JSONArray>(){
                    @Override
                    public void onResponse(JSONArray response) {
                        progressBar.setVisibility(View.INVISIBLE);

                        Log.d("currentFragment", "response generated" + response.toString());
                        try {
//                            for(int i =0; i < response.length(); i++) {

                            JSONObject jsonObject = response.getJSONObject(0);
                            Log.d("test", "0 = " + jsonObject.get("ticker").toString());
                            StockDetail.clear();
                            StockDetail.add("Ticker: ", jsonObject.get("ticker").toString());
                            StockDetail.add("TimeStamp: ", jsonObject.get("timestamp").toString());
                            StockDetail.add("Previous Close: ", ifNull(jsonObject.get("prevClose").toString()));
                            StockDetail.add("Ask Price: ", ifNull(jsonObject.get("askPrice").toString()));
                            StockDetail.add("Last: ", ifNull(jsonObject.get("last").toString()));
                            StockDetail.add("High: ", ifNull(jsonObject.get("high").toString()));
                            StockDetail.add("Low: ", ifNull(jsonObject.get("low").toString()));

                            RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.stockDetailRecyclerView);
                            CurrentStockAdapter listAdapter = new CurrentStockAdapter();
                            recyclerView.setAdapter(listAdapter);
                            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
                            recyclerView.setLayoutManager(layoutManager);
//                            }
                        } catch (JSONException e) {

                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
//        arrayRequest.setRetryPolicy(new DefaultRetryPolicy(
//                20000,
//                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
//                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.d("currentFragment","added to requestQueue");
        requestQueue.add(arrayRequest);
    }

    private String ifNull(String s){
        if(s.equals("null"))
            return "-";
        return s;
    }

}