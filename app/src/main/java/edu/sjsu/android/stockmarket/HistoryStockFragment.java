package edu.sjsu.android.stockmarket;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class HistoryStockFragment extends Fragment {
    RequestQueue requestQueue;
    ProgressBar progressBar;
    View view;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_history_stock, container, false);
        progressBar = (ProgressBar) getActivity().findViewById(R.id.progress_circular);
        progressBar.setVisibility(View.VISIBLE);

        requestQueue = Volley.newRequestQueue(getActivity().getApplicationContext());
//        Log.d("bundle received", "result: " + getArguments().getString("ticker"));
//        String ticker = getArguments().getString("ticker");
//        Log.d("ticker request", "\""+ticker+"\"");
        String ticker = StockActivity.ticker;
        StockHistoryList.historyArrayList.clear();
        setStockHistory(ticker);

        // Inflate the layout for this fragment


        return view;
    }

    private void setStockHistory(String s) {
        String token = "3ed1f372b5a83348867fe3a87aca6d1b2e569ab2";
        String date= "2020-01-02";
        String freq = "monthly";
        String url = "https://api.tiingo.com/tiingo/daily/" + s + "/prices?startDate=" +date+"&resampleFreq="+freq+"&token=" + token;
        Log.d("quote", "about to reach onResponse");
        JsonArrayRequest arrayRequest = new JsonArrayRequest(url,
                new Response.Listener<JSONArray>(){
                    @Override
                    public void onResponse(JSONArray response) {
                        progressBar.setVisibility(View.INVISIBLE);
                        Log.d("historyFragment", "response generated" + response.toString());
                        try{
                            for(int i = 0; i < response.length(); i++){
                                JSONObject jsonObject = response.getJSONObject(i);
                                Log.d("historyFragment",jsonObject.get("adjVolume").toString());
                                StockHistory temp = new StockHistory(jsonObject.get("date").toString(), jsonObject.get("close").toString(), jsonObject.get("volume").toString());
                                StockHistoryList.historyArrayList.add(temp);
                            }
                        }catch(JSONException e){
                            e.printStackTrace();
                        }
                        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.stockHistoryRecyclerView);
                        HistoryStockAdapter listAdapter = new HistoryStockAdapter();
                        recyclerView.setAdapter(listAdapter);
                        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
                        recyclerView.setLayoutManager(layoutManager);
                    }
                }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
//        arrayRequest.setRetryPolicy(new DefaultRetryPolicy(
//                20000,
//                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
//                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.d("historyFragment","added to requestQueue");
        requestQueue.add(arrayRequest);
    }
}

