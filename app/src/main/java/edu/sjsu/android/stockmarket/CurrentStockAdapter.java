package edu.sjsu.android.stockmarket;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class CurrentStockAdapter extends RecyclerView.Adapter {

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Log.d("CSA","CurrentStockAdapter called");
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.stock_detail_row, parent, false);
        return new CurrentStockViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ((CurrentStockViewHolder) holder).bindView(position);
    }

    @Override
    public int getItemCount() {
        return StockDetail.getLength();
    }

    private class CurrentStockViewHolder extends RecyclerView.ViewHolder {
        private TextView label;
        private TextView content;

        public CurrentStockViewHolder(View itemView){
            super(itemView);
            label = (TextView) itemView.findViewById(R.id.stock_label);
            content = (TextView) itemView.findViewById(R.id.stock_content);

        }

        public void bindView(int position){
            Log.d("bind","binded called " + StockDetail.labelList.get(position));
            label.setText(StockDetail.labelList.get(position));
            content.setText(StockDetail.contentList.get(position));
        }

    }
}
