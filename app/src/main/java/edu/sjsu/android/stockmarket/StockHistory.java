package edu.sjsu.android.stockmarket;

public class StockHistory {
    private String date;
    private String closePrice;
    private String volume;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getClosePrice() {
        return closePrice;
    }

    public void setClosePrice(String closePrice) {
        this.closePrice = closePrice;
    }

    public String getVolume() {
        return volume;
    }

    public void setVolume(String volume) {
        this.volume = volume;
    }

    public StockHistory() {
    }

    public StockHistory(String date, String closePrice, String volume) {
        this.date = date;
        this.closePrice = closePrice;
        this.volume = volume;
    }
}
